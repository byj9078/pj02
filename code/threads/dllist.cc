#include <iostream>
#include "dllist.h"
using namespace std;

// initialize a list element
DLLElement::DLLElement( void *itemPtr, int sortKey )
{
  item = itemPtr;
  key = sortKey;
  next = NULL;
  prev = NULL;
}


void
DLList::deleteElement(DLLElement *element) {
  if(!IsEmpty() & element != NULL) {
    deleteElement(element->next);
    delete element->item;
    delete element;
  }
}

// initialize the list
DLList::DLList()
{
  first = NULL;
  last = NULL;
  cout << "I'm Allocator DLList!" << endl;
}

// de-allocate the list
DLList::~DLList()
{
  cout << "~DLList()" << endl;
  deleteElement(first);
}

// return the true if list has no elements
bool
DLList::IsEmpty()
{
  if(first == NULL)
    return true;
  else
    return false;
}

void
DLList::Prepend(void *item) {
  cout << "Prepend()" << endl;
  if(IsEmpty()) {
    //cout << "This is Empty case" << endl;
    DLLElement *new_element = new DLLElement(item, 0);
    first = new_element;
    last = new_element;
  }
  else {
    //cout << "This is NON_EMPTY case" << endl;
    DLLElement *new_element = new DLLElement(item, first->key - 1); // set min_key - 1
    new_element->next = first;
    first->prev = new_element;
    first = new_element;
  }
  cout << "first item:" << *(int*)(first->item) << endl;
  cout << "last item:" << *(int*)(last->item) << endl;
}

void
DLList::Append(void *item) {
  cout << "Append" << endl;
  if(IsEmpty()) {
    //cout << "This is Empty case" << endl;
    DLLElement *new_element = new DLLElement(item, 0);
    first = new_element;
    last = new_element;
  }
  else {
    //cout << "This is NON_EMPTY case" << endl;
    DLLElement *new_element = new DLLElement(item, last->key + 1);
    new_element->prev = last;
    last->next = new_element;
    last = new_element;
  }
  cout << "first item:" << *(int*)(first->item) << endl;
  cout << "last item:" << *(int*)(last->item) << endl;
}

void*
DLList::Remove(int *keyPtr) {
  //cout << "Remove()" << endl;
  if(IsEmpty()) {
    //cout << "The list is EMPTY!" << endl;
    return NULL;
  }

  // save the first thing...
  DLLElement *element = first;
  void *remove_item;
  remove_item = element->item;

  // # of element is 1
  if (first == last) {
    //cout << "first == last" << endl;
    first = NULL;
    last = NULL;
  }

  // remove from head of list
  else {
    //cout << "ELSE" << endl;
    first = first->next;
    first->prev = NULL;
    //cout << "first item:" << *(int*)(first->item) << endl;
    //cout << "last item:" << *(int*)(last->item) << endl;
  }

  if(keyPtr != NULL) {
    //cout << "keyPtr != NULL" << endl;
    *keyPtr = element->key;
  }

  return remove_item;
}

// put items on list in order (sorted by key)
void
DLList::SortedInsert(void *item, int sortKey) {
  DLLElement *new_element = new DLLElement(item, sortKey);
  if (IsEmpty()) {
    first = new_element;
    last = new_element;
  }
  else if (sortKey < first->key) {
    // sortKey is min_key
    //cout << "sortKey is min" << endl;
    new_element->next = first;
    first->prev = new_element;
    first = new_element;
  }
  else {
    for(DLLElement *ptr = first; ptr->next != NULL; ptr = ptr->next) {
      if (sortKey < ptr->next->key) {
        // new_element insert between ptr, ptr->next
        new_element->next = ptr->next;
        new_element->prev = ptr;
        ptr->next->prev = new_element;
        ptr->next = new_element;
        return;
      }
    }
    // sortKey is max_key
    last->next = new_element;
    new_element->prev = last;
    last = new_element;
  }
}

void*
DLList::SortedRemove(int sortKey) {
  if(IsEmpty()) {
    cout << "EMPTY!" << endl;
    return NULL;
  }

  DLLElement *ptr = first;

  // search key == sortKey
  while(ptr != NULL) {
    cout << "Searching.." << endl;
    if(sortKey == ptr->key) {

      if(ptr == first) {
        if(first == last) { // # of element is 1
          first = NULL;
          last = NULL;
        }
        else {
          first = ptr->next;
          first->prev = NULL;
        }
      }
      else if(ptr == last) {
        last = ptr->prev;
        last->next = NULL;
      }
      else {
        ptr->prev->next = ptr->next;
        ptr->next->prev = ptr->prev;
      }
      return ptr;
    }
    ptr = ptr->next;
  }
    //cout << "NOT EXIST!" << endl;
    return NULL;
}

void
DLList::PrintList() {
  DLLElement *ptr = first;
  while(ptr != NULL) {
    cout << "(" << ptr->key << ":" << *(int*)(ptr-> item) << ")-";
    ptr = ptr->next;
  }
  cout << endl;
}
