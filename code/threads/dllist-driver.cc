#include <iostream>
#include <stdlib.h>
#include "dllist.h"
#include "thread.h"
#include "debug.h"
#include "main.h"

using namespace std;

int N;
DLList *dllist = new DLList();

void InsertNItemsDLList(DLList *list, int N, int which) {
  for(int i = 0; i < N; i++) {
    int key = rand() % 101;
    int *item = new int (rand() % 101);
    list->SortedInsert(item, key);
    cout << "Thread" << which << "] key: " << key << " item: " << *item << " inserted!" << endl;
  }
}
void RemoveNItemsDLList(DLList *list, int N, int which) {
  int key;
  for(int i = 0; i < N; i++) {
    int *result = (int*)list->Remove(&key);
    if(result != NULL)
      cout << "\t\tThread" << which << "] key: " << key << " removed!" << endl;
    else
      cout << "List is already empty" << endl;
    //list->PrintList();
  }
}

void SortedInsertAndRemove(int i) {
  //cout << "SortedInsertNRemove" << endl;
  InsertNItemsDLList(dllist, N, i);
  kernel->currentThread->Yield();
  RemoveNItemsDLList(dllist, N, i);
}

void HW1Test(int num_Thread, int num_N) {
    N = num_N;
    SortedInsertAndRemove(0);
    for(int i = 1; i < num_Thread; i++) {
      Thread *t = new Thread("forked thread");
      t->Fork((VoidFunctionPtr)SortedInsertAndRemove, (void*)i);
    }
}
